export {
  defineComponent,
  ref,
  computed,
  onMounted,
  onDeactivated,
  onUpdated,
  onBeforeUpdate
} from 'vue';
export { useStore } from '@/store';
export { useRoute } from 'vue-router'
