import { setup } from 'axios-cache-adapter'
import { store } from './index'
import { DiscogsReleases, DiscogsWishlist, Release, User } from "@/types/discogs";
import { Axios, AxiosResponse } from 'axios';

const client = setup({
  baseURL: 'https://api.discogs.com',
  timeout: 10000,
  headers: { 'Authorization': 'Discogs token=dRBtnEnVxpHwBwOkvLmIGEFlUXxmIEzIuLtkyReL' },
  cache: {
    maxAge: 15 * 60 * 1000,
    exclude: {
      query: false
    },
    invalidate: async (config, request) => {
      if (request.clearCacheEntry) {
        // @ts-ignore
        await config.store.removeItem(config.uuid)
      }
    }
  }
})

const getUser = async (userName: string):Promise<User> => {
  return await client.get<User>(`/users/${userName}`).then(({ data }) => {
    store.commit('setState', {
      path: 'user',
      value: data
    })
    return data
  })
}

const getReleases = async ():Promise<Release[]> => {
  const userName = store.state.user?.username
  const pagination = store.state.pagination
  const page = pagination && pagination.page < pagination.pages ? pagination.page + 1 : pagination && pagination.page === pagination.pages ? 0 : 1;
  return userName && page ? await client.get<DiscogsReleases>(`/users/${userName}/collection/folders/0/releases?page=${page}&per_page=20`).then(({ data }) => {
    const releases = data.releases
    const prevReleases = store.state.records
    store.commit('setState', {
      path: 'pagination',
      value: data.pagination
    })
    store.commit('setState', {
      path: 'records',
      value: [
        ...prevReleases,
        ...releases
      ]
    })
    return releases
  }) : Promise.resolve([])
}

const cleanRecords = () => {
  store.commit('setState', {
    path: 'pagination',
    value: null
  })
  store.commit('setState', {
    path: 'records',
    value: []
  })
}

const getWishList = async ():Promise<Release[]> => {
  const userName = store.state.user?.username
  return userName ? await client.get<DiscogsWishlist>(`/users/${userName}/wants?per_page=999`).then(({ data }) => {
    const wishlist = data.wants
    store.commit('setState', {
      path: 'wishlist',
      value: wishlist
    })
    return wishlist
  }) : []
}

export { getReleases, getWishList, getUser, cleanRecords }
