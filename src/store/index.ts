import { InjectionKey } from 'vue'
import { createStore, useStore as baseUseStore, Store } from 'vuex'
import { Release, User, DiscogsPagination } from '@/types/discogs'
import set from 'lodash.set'

export interface State {
  user: User | null,
  records: Release[],
  pagination: DiscogsPagination|null,
  wishlist: Release[]
}

export const key: InjectionKey<Store<State>> = Symbol()

export const store = createStore<State>({
  state: {
    user: null,
    records: [],
    pagination: null,
    wishlist: []
  },
  mutations: {
    setState(state, { path, value }) {
      set(state, path, value)
    }
  }
})

export function useStore () {
  return baseUseStore(key)
}
