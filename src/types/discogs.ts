export interface DiscogsReleases {
  pagination: DiscogsPagination;
  releases: Release[]
}

export interface DiscogsWishlist {
  pagination: DiscogsPagination;
  wants: Release[]
}

export interface DiscogsPagination {
  items: number;
  page: number;
  pages: number;
  per_page: number;
  urls: any
}

export interface Release {
  id:                number;
  instance_id:       number;
  date_added:        Date;
  rating:            number;
  basic_information: BasicInformation;
}

export interface BasicInformation {
  id:           number;
  master_id:    number;
  master_url:   string;
  resource_url: string;
  thumb:        string;
  cover_image:  string;
  title:        string;
  year:         number;
  formats:      Format[];
  labels:       Label[];
  artists:      Artist[];
  genres:       string[];
  styles:       string[];
}

export interface Artist {
  name:         string;
  anv:          string;
  join:         string;
  role:         string;
  tracks:       string;
  id:           number;
  resource_url: string;
}

export interface Format {
  name:         string;
  qty:          string;
  text:         string;
  descriptions: string[];
}

export interface Label {
  name:             string;
  catno:            string;
  entity_type:      string;
  entity_type_name: string;
  id:               number;
  resource_url:     string;
}

export interface User {
  id:                     number;
  resource_url:           string;
  uri:                    string;
  username:               string;
  name:                   string;
  home_page:              string;
  location:               string;
  profile:                string;
  registered:             Date;
  rank:                   number;
  num_pending:            number;
  num_for_sale:           number;
  num_lists:              number;
  releases_contributed:   number;
  releases_rated:         number;
  rating_avg:             number;
  inventory_url:          string;
  collection_folders_url: string;
  collection_fields_url:  string;
  wantlist_url:           string;
  avatar_url:             string;
  curr_abbr:              string;
  activated:              boolean;
  marketplace_suspended:  boolean;
  banner_url:             string;
  buyer_rating:           number;
  buyer_rating_stars:     number;
  buyer_num_ratings:      number;
  seller_rating:          number;
  seller_rating_stars:    number;
  seller_num_ratings:     number;
  is_staff:               boolean;
  num_collection:         number;
  num_wantlist:           number;
}
