import LazyLoad from 'vanilla-lazyload';
import { Directive } from 'vue';

export const lazy: Directive = {
  beforeMount(el) {
    // @ts-ignore
    el.lazy = new LazyLoad({}, [el])
  },
  unmounted(el) {
    if (el.lazy) {
      el.lazy.destroy()
    }
  }
}
